# -----------------------------------------------------------
# Defines certain parameters, tests neural networks in regards to those parameters
# and saves results as .png and .csv files
#
# (C) 2020 Ivan Stresec
# email ivan.stresec@gmail.com
# -----------------------------------------------------------
import matplotlib.pyplot as plt
import sys
import numpy as np
import pandas as pd
import tensorflow as tf
from tensorflow import keras
from tensorflow.keras import layers
from data_manipulation.dataset import Dataset
from model.model import Model


def build_and_compile_model(layers_list, opt, act):
    """Builds and compiles a sequential Keras NN model using given parameters

     :param layers_list: integer list with number of neurons for each hidden layer
     :param opt: a Keras optimizer to be used in model
     :param act: activation function to be used by layers
    """
    # define first hidden layer with input
    temp_layers = [
        layers.Dense(layers_list[0], activation=act, input_shape=[len(train_data.keys())])]

    # define other hidden layers
    for neuron_num in layers_list[1:]:
        temp_layers.append(layers.Dense(neuron_num, activation=act))

    # create keras NN model
    temp_model = keras.Sequential()
    for layer in temp_layers:
        temp_model.add(layer)
        temp_model.add(layers.BatchNormalization())
    temp_model.add(layers.Dense(1))

    temp_model.compile(loss='mse', optimizer=opt, metrics=['mae', 'mse'])

    return temp_model


# constants
TRAIN_INPUT_PATH = 'raw_data/train_dataset.csv'
VALIDATION_INPUT_PATH = 'raw_data/validation_dataset.csv'
TEST_INPUT_PATH = 'raw_data/test_dataset.csv'

# read data, generated by raw_data/data_filter_and_split.py
train_data = pd.read_csv(filepath_or_buffer=TRAIN_INPUT_PATH, index_col=0)
validation_data = pd.read_csv(filepath_or_buffer=VALIDATION_INPUT_PATH, index_col=0)
test_data = pd.read_csv(filepath_or_buffer=TEST_INPUT_PATH, index_col=0)

# remove mobility values (the ones we predict) and
# save them into separate variables
train_labels = train_data.pop('mob')
validation_labels = validation_data.pop('mob')
test_labels = test_data.pop('mob')
# print(test_data)
# print(validation_data)
# print(test_labels)

# get statistics for normalization
train_data_stats = train_data.describe()
train_data_stats = train_data_stats.transpose()

# normalize the data using statistics of training data
normed_train_data = Dataset.normalize(train_data, train_data_stats)
normed_validation_data = Dataset.normalize(validation_data, train_data_stats)
normed_test_data = Dataset.normalize(test_data, train_data_stats)

# define optimizers
optimizers = []
opt_rms_prop = keras.optimizers.RMSprop(learning_rate=0.0005, rho=0.9)
optimizers.append(opt_rms_prop)
opt_adam = keras.optimizers.Adam(learning_rate=0.0005, beta_1=0.9, beta_2=0.999, amsgrad=True)
optimizers.append(opt_adam)
opt_adadelta = keras.optimizers.Adadelta(learning_rate=1.0, rho=0.95)
optimizers.append(opt_adadelta)
opt_adagrad = keras.optimizers.Adagrad()
optimizers.append(opt_adagrad)
opt_sgd = keras.optimizers.SGD(learning_rate=0.01, momentum=0, nesterov=False)
optimizers.append(opt_sgd)
opt_nadam = keras.optimizers.Nadam(learning_rate=0.0005)

# define epochs
epochs = 2000

# define activation function
# act_relu = 'relu'
# activation_function = act_relu
# act_lrelu = keras.layers.LeakyReLU(alpha=0.05)
# activation_function = act_lrelu
# activation_function_string = 'LeakyReLU(alpha=0.05)'
act_sigmoid = 'sigmoid'
activation_function = act_sigmoid
# act_tanh = 'tanh'
# activation_function = act_tanh
# act_softmax = 'softmax'
# activation_function = act_softmax
# act_softplus = 'softplus'
# activation_function = act_softplus
# act_softsign = 'softsign'
# activation_function = act_softsign
# act_elu = 'elu'
# activation_function = act_elu
# act_selu = 'selu'
# activation_function = act_selu
# act_hard_sigmoid = 'hard_sigmoid'
# activation_function = act_hard_sigmoid
# act_linear = 'linear'
# activation_function = act_linear

number_of_neurons = int(sys.argv[1])
number_of_layers = int(sys.argv[2])
layer_list = []
for i in range(number_of_layers):
    layer_list.append(number_of_neurons)
save_folder = 'architecture_optimization'

# MAIN
# builds and compiles NN models, trains them and saves data

# insure reproducibility (program or script must be run with PYTHONHASHSEED environment variable set to 0)
np.random.seed(0)
tf.compat.v1.set_random_seed(0)
session_conf = tf.compat.v1.ConfigProto(intra_op_parallelism_threads=1,
                                        inter_op_parallelism_threads=1)
sess = tf.compat.v1.Session(graph=tf.compat.v1.get_default_graph(), config=session_conf)
tf.compat.v1.keras.backend.set_session(sess)

# construct model and train it
tf_model = build_and_compile_model(layers_list=layer_list, opt=opt_adam, act=activation_function)
model = Model(tf_model=tf_model, optimizer=opt_adam, save_folder=save_folder)
model.set_train_data(train_data=normed_train_data, train_labels=train_labels)
model.set_validation_data(validation_data=validation_data, validation_labels=validation_labels)
model.train(epochs=epochs)

# saving training data
history = pd.DataFrame(model.history.history)
history.to_csv(model.file_path + '/history.csv')

# test original and save data
test_predictions = model.predict(test_data=normed_test_data)
model.plot_prediction(test_labels, test_predictions)
test_predictions_csv = pd.DataFrame(pd.Series(test_predictions, name='predicted_mobility'))
test_predictions_csv.to_csv(model.file_path + '/predicted_mobility.csv')

# save error data
error = test_labels - test_predictions
model.plot_error(error)
error = [abs(x) for x in error]
error = pd.DataFrame(error)
error_stats = error.describe().transpose()
print(error_stats['mean'].get_value(0, 'mean'))
with open(model.file_path + '/' + str(error_stats['mean'].get_value(0, 'mean')), 'w+'):
    pass
error.to_csv(model.file_path + '/prediction_error.csv')

# save true and prediction data in one .csv
true_predicted = pd.DataFrame()
true_predicted['true'] = test_labels
true_predicted['predicted'] = test_predictions
true_predicted.to_csv(model.file_path + '/true_predicted.csv')

with open('architecture_optimization.csv', 'a') as f:
    if isinstance(activation_function, str):
        f.write(activation_function + ',' + str(error_stats['mean'].get_value(0, 'mean')) + ',' + str(
            layer_list[0]) + ',' + str(len(layer_list)) + '\n')
    else:
        f.write(activation_function_string + ',' + str(error_stats['mean'].get_value(0, 'mean')) + ',' + str(
            layer_list[0]) + ',' + str(len(layer_list)) + '\n')
        exit(1)
