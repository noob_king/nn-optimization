import os

layer_arguments = [16]
neuron_arguments = [48]
neuron_argument = 0
layer_argument = 0
for i in neuron_arguments:
    neuron_argument = i
    for j in layer_arguments:
        if i == 1 and j != 1:
            continue
        layer_argument = j
        command = 'python tester.py ' + str(neuron_argument) + ' ' + str(layer_argument)
        print('Running:\n', command)
        os.system(command)
