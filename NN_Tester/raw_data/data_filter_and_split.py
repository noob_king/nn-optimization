# -----------------------------------------------------------
# Filters and then splits raw SiNR transistor data stored in a .csv file into two separate .csv files,
# each tenth sample of the sorted data is used for testing
#
# (C) 2020 Ivan Stresec
# email ivan.stresec@gmail.com
# -----------------------------------------------------------
import pandas as pd

# constants
INPUT_PATH = 'test_data.csv'

# read data into Pandas DataFrame, remove extra column
data = pd.read_csv(filepath_or_buffer=INPUT_PATH)
# data.pop('G[off]')  # remove G[off]

# filter unrealistic data
data = data[lambda x: x['E[g]'] < 1.0]  # remove E[g] > 1
data = data[lambda x: x['mob'] < 180.0]  # remove mob > 180
data = data.sort_values(by=['mob'])
print('Sorted:\n' + str(data), '\n')

print('All data:\n------------------\n' + str(data))
stats = data.describe()
print(stats, '\n\n')

# take each fifth sorted sample for testing
test_data = data.iloc[::5, :]
# train_data = data.sample(frac=PERCENTAGE, random_state=RANDOM_SEED)
train_data = data.copy()
train_data = train_data.drop(test_data.index)

# take each fifth for validation
validation_data = train_data.iloc[::5, :]
train_data = train_data.drop(validation_data.index)

# print statistics
print('Train data:\n------------------\n' + str(train_data))
stats = train_data.describe()
print(stats, '\n\n')
print('Validation data:\n------------------\n' + str(validation_data))
stats = validation_data.describe()
print(stats, '\n\n')
print('Test data:\n------------------\n' + str(test_data))
stats = test_data.describe()
print(stats, '\n\n')

# shuffle
train_data = train_data.sample(frac=1)
validation_data = validation_data.sample(frac=1)
test_data = test_data.sample(frac=1)
print('Shuffled train data:\n------------------\n' + str(train_data))
print('Shuffled validation data:\n------------------\n' + str(validation_data))
print('Shuffled test data:\n------------------\n' + str(test_data))

# save split data to .csv
train_data.to_csv('train_dataset.csv')
validation_data.to_csv('validation_dataset.csv')
test_data.to_csv('test_dataset.csv')
