# -----------------------------------------------------------
# Contains a class for storing and manipulating data which is used in training
# and validating TensorFlow neural networks (sequential models)
#
# (C) 2020 Ivan Stresec
# email ivan.stresec@gmail.com
# -----------------------------------------------------------
import pandas as pd


class Dataset:
    """Represents data to be used by neural network

    Contains some functions for data manipulation
    """

    def __init__(self, path):
        self.raw_data = pd.read_csv(filepath_or_buffer=path)
        self.normalized_data = None

    def filter(self, cond):
        """Filters data from object

        :param cond: a condition for filtering
        """
        self.raw_data = self.raw_data[cond]

    @staticmethod
    def normalize(data: pd.DataFrame, stats=None) -> pd.DataFrame:
        """Normalize a Pandas DataFrame

        :param data: Pandas DataFrame object
        :param stats: Pandas DataFrame object containing statistics for normalization
        :return: normalized Pandas DataFrame object
        """
        return (data - stats['mean']) / stats['std']
