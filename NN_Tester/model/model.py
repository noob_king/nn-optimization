# -----------------------------------------------------------
# Contains a class which represents the model of a neural network, it's defined
# depending on the parameters given and contains functions for its training
# and validation
#
# (C) 2020 Ivan Stresec
# email ivan.stresec@gmail.com
# -----------------------------------------------------------
import tensorflow as tf
from tensorflow import keras
import matplotlib.pyplot as plt
import seaborn as sns
import pandas as pd
import os
import time

TOP_SAVE_FOLDER = 'results'


class Model:
    """Represents a neural network model

    Contains a TensorFlow model as well as other
    data concerning it's training and validation
    """

    def __init__(self, tf_model, optimizer, save_folder=''):
        self.model = tf_model
        self.file_path = self.__configure_path(optimizer, save_folder)
        self.train_data = None
        self.train_labels = None
        self.validation_data = None
        self.validation_labels = None
        self.epochs = None
        self.history = None
        self.train_time = None

    def __configure_path(self, optimizer, save_folder=''):
        """ Configures path for saving data from a certain model

        :param optimizer: optimizer used
        :param save_folder: optional folder name
        :return: string path
        """

        temp_path = TOP_SAVE_FOLDER
        # creates the top folder
        try:
            os.mkdir(temp_path)
        except OSError:
            pass

        temp_path += '/' + save_folder
        # creates folder defined by function parameter inside top folder
        try:
            os.mkdir(temp_path)
        except OSError:
            pass

        if not temp_path.endswith('/'):
            temp_path += '/'

        config = self.model.get_config()
        temp_path += 'layers' + str((len(config['layers']) - 1)//2) + '_neurons' + str(
            config['layers'][0]['config']['units']) + '_'
        # temp_path += 'input(3)_'
        # for layer in config['layers'][:-1]:
        #     if layer['class_name'] == 'BatchNormalization':
        #         continue
        #     temp_path = temp_path + str(layer['config']['units']) + '_'
        if isinstance(config['layers'][0]['config']['activation'], str):
            temp_path += config['layers'][0]['config']['activation'] + '_' + type(optimizer).__name__
        else:
            temp_path += config['layers'][0]['config']['activation']['class_name'] + '_' + type(optimizer).__name__
        # creates a folder for the specific model
        try:
            os.mkdir(temp_path)
        except OSError:
            pass  # ignore if folder already exists

        return temp_path

    def set_train_data(self, train_data, train_labels):
        self.train_data = train_data
        self.train_labels = train_labels

    def set_validation_data(self, validation_data, validation_labels):
        self.validation_data = validation_data
        self.validation_labels = validation_labels

    def train(self, epochs):
        """Trains model and saves data

        :param epochs: number of training epochs
        :param val_split: percentage of data used for validation
        """
        self.epochs = epochs
        start_time = time.time()

        # define early stop method, training stops if validation loss doesn't decrease for 400 epochs
        early_stopper = keras.callbacks.EarlyStopping(monitor='val_loss', patience=400, restore_best_weights=True)
        self.history = self.model.fit(
            x=self.train_data, y=self.train_labels,
            epochs=epochs, validation_data=(self.validation_data, self.validation_labels), verbose=0, batch_size=128,
            callbacks=[self.ProgressPrinter(), early_stopper], shuffle=True
        )
        print(']\n')  # addition to ProgressPrinter callback class
        self.train_time = time.time() - start_time
        self.plot_history()

    def predict(self, test_data):
        return self.model.predict(test_data).flatten()

    def plot_history(self):
        # Mean absolute error history
        hist = pd.DataFrame(self.history.history)
        hist['epoch'] = self.history.epoch

        plt.figure(figsize=(12, 12))
        plt.xlabel('Epoch')
        plt.ylabel('Mean Abs Error [mob]')
        plt.plot(hist['epoch'], hist['mae'],
                 label='Train Error')
        plt.plot(hist['epoch'], hist['val_mae'],
                 label='Val Error')
        plt.xlim([plt.xlim()[0], plt.xlim()[1]])
        plt.ylim([plt.ylim()[0], plt.ylim()[1]])
        plt.legend()
        plt.savefig(self.file_path + '/mae_graph_' + '.png',
                    bbox_inches='tight')
        plt.close()
        plt.clf()

        # Mean square error history
        plt.figure(figsize=(12, 12))
        plt.xlabel('Epoch')
        plt.ylabel('Mean Square Error [$mob^2$]')
        plt.plot(hist['epoch'], hist['mse'],
                 label='Train Error')
        plt.plot(hist['epoch'], hist['val_mse'],
                 label='Val Error')
        plt.xlim([plt.xlim()[0], plt.xlim()[1]])
        plt.ylim([plt.ylim()[0], plt.ylim()[1]])
        plt.legend()
        plt.savefig(self.file_path + '/mse_graph_' + '.png',
                    bbox_inches='tight')
        plt.close()
        plt.clf()

    def plot_prediction(self, test_labels, test_predictions):
        # Prediction
        plt.figure(figsize=(12, 12))
        plt.scatter(test_labels, test_predictions)
        plt.xlabel('True Values [mob]')
        plt.ylabel('Predictions [mob]')
        plt.axis('equal')
        plt.axis('square')
        plt.xlim([plt.xlim()[0], plt.xlim()[1]])
        plt.ylim([plt.ylim()[0], plt.ylim()[1]])
        plt.plot([-1000, 1000], [-1000, 1000])
        plt.savefig(self.file_path + '/prediction_graph' + '.png',
                    bbox_inches='tight')
        plt.close()
        plt.clf()

    def plot_error(self, error):
        # error histogram
        plt.figure(figsize=(12, 12))
        plt.hist(error, bins=50)
        plt.xlabel('prediction error')
        plt.ylabel('count')
        plt.xlim([plt.xlim()[0], plt.xlim()[1]])
        plt.ylim([plt.ylim()[0], plt.ylim()[1]])
        plt.savefig(self.file_path + '/error_hist' + '.png',
                    bbox_inches='tight')
        plt.close()
        plt.clf()

    class ProgressPrinter(keras.callbacks.Callback):
        """Callback class for TensorFlow model training/fitting

        Contains a static method for training feedback output
        """

        @staticmethod
        def on_epoch_end(epoch, logs):
            """Generates output for epochs of training

            :param epoch: current epoch number
            :param logs: not used
            """
            if epoch == 0:
                print('[', end='')
            elif epoch > 1 and epoch % 100 == 0:
                print(']\n[', end='')
            print('=', end='')
