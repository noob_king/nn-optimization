# NN optimization

Optimization of neural network architecture for predicting electron mobility in SiNR transistors using Python TensorFlow (and related data manipulation libraries). 
Research for a bachelor thesis.
For reproducibility use PYTHONHASHSEED=0 when running tester or any script running the tester.
Tester shouldn't contain loops for testing various NNs  with reproducibility since this changes the random seed.
(C) Ivan Stresec 2020